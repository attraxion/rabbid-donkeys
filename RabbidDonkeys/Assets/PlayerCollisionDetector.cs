﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollisionDetector : MonoBehaviour {

	private BoxCollider _collider;
	// Use this for initialization
	void Start () {
		_collider = GetComponent<BoxCollider>();
	}

	private void OnCollisionEnter(Collision other) {
		Debug.Log("Box collider collided with: " + other.gameObject.name);
	}

}
