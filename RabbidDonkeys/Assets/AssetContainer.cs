﻿using System.Collections;
using System.Collections.Generic;
using Managers;
using UnityEngine;

public class AssetContainer : MonoBehaviour{
    // Use this for initialization
    public void RegisterToGM(){
        switch (gameObject.name) {
            case "VILLAGE":
                GameManager.Instance.VillageGO = gameObject;
                Debug.Log("Registered to GM: " + gameObject.name);
                break;
            case "FOREST":
                GameManager.Instance.ForestGO = gameObject;
                Debug.Log("Registered to GM: " + gameObject.name);
                break;
            case "YOMI":
                GameManager.Instance.YomiGO = gameObject;
                Debug.Log("Registered to GM: " + gameObject.name);
                break;
            case "FUJI":
                GameManager.Instance.FujiGO = gameObject;
                Debug.Log("Registered to GM: " + gameObject.name);
                break;
        }
    }
}