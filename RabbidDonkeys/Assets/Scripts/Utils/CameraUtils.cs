﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utils {

    public class CameraUtils : MonoBehaviour {

        private static CameraUtils _instance;
        public static CameraUtils Instance { get { return _instance; } }

        private void Awake() {
            if (_instance == null) {
                _instance = this;
            }
        }

        public bool IsInView(GameObject origin, GameObject toCheck) {
            Vector3 pointOnScreen = Camera.main.WorldToScreenPoint(toCheck.GetComponentInChildren<Renderer>().bounds.center);

            //Is in front
            if (pointOnScreen.z < 0) {
                Debug.Log("Behind: " + toCheck.name);
                return false;
            }

            //Is in FOV
            if ((pointOnScreen.x < 0) || (pointOnScreen.x > Screen.width) ||
                (pointOnScreen.y < 0) || (pointOnScreen.y > Screen.height)) {
                Debug.Log("OutOfBounds: " + toCheck.name);
                return false;
            }

            RaycastHit hit;
            Vector3 heading = toCheck.transform.position - origin.transform.position;
            Vector3 direction = heading.normalized; // / heading.magnitude;

            if (Physics.Linecast(Camera.main.transform.position, toCheck.GetComponentInChildren<Renderer>().bounds.center, out hit)) {
                if (hit.transform.name != toCheck.name) {
                    /* -->
                    Debug.DrawLine(cam.transform.position, toCheck.GetComponentInChildren<Renderer>().bounds.center, Color.red);
                    Debug.LogError(toCheck.name + " occluded by " + hit.transform.name);
                    */
                    Debug.Log(toCheck.name + " occluded by " + hit.transform.name);
                    return false;
                }
            }
            return true;
        }
    }
}