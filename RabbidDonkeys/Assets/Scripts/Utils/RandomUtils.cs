﻿using System.Collections.Generic;
using UnityEngine;

namespace Utils {
    public static class RandomUtils {

        public static T GetRandomListElement<T>(List<T> list) {
            return list[Random.Range(0, list.Count - 1)];
        }

    }
}