﻿namespace Managers.Interfaces {
    public interface IStateManager<in T> {
        void SetState(T state);
    }
}