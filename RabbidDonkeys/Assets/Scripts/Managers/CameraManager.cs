﻿using System.Collections.Generic;
using Player;
using UnityEngine;
using UnityEngine.PostProcessing;

namespace Managers {

    public enum CameraPostProcessingEnum {
        NORMAL = 0,
        DARK = 1,
        DARKEST = 2,
        TRANSITION = 3
    }

    public class CameraManager : MonoBehaviour {
        private static CameraManager _instance;
        public static CameraManager Instance { get { return _instance; } }
        [SerializeField] private GameObject _target;
        [SerializeField] private float _offset;
        [SerializeField] private PostProcessingProfile _currentPostProcessingProfile;
        [SerializeField] private CameraPostProcessingEnum _currentProfileEnum;
        [SerializeField] private CameraPostProcessingEnum _previousProfileEnum;
        [SerializeField] private List<PostProcessingProfile> _allPostProcessingProfiles;

        private Camera _camera;
        private CameraPostProcessingEnum _defaultCameraPostProcessingEnum = CameraPostProcessingEnum.NORMAL;
        private PostProcessingBehaviour _currentPostProcessingBehaviour;

        private TimePostProcessingTransition _postProcessingTransition;
        
        private void Awake() {
            if (_instance == null) {
                _instance = this;
            }

            _camera = GetComponent<Camera>();

            _currentPostProcessingBehaviour = GetComponent<PostProcessingBehaviour>();
            _currentPostProcessingProfile = _currentPostProcessingBehaviour.profile;
            _currentProfileEnum = _defaultCameraPostProcessingEnum;
            _postProcessingTransition = GetComponent<TimePostProcessingTransition>();
        }

        void Update() {
            if (PlayerStateManager.Instance.currentPlayerState == PlayerState.TRANSITION && _currentProfileEnum != CameraPostProcessingEnum.TRANSITION) {
                SetNewCameraProfile(_currentPostProcessingProfile, CameraPostProcessingEnum.TRANSITION);
            } else if (PlayerStateManager.Instance.currentPlayerState != PlayerState.TRANSITION && _currentProfileEnum == CameraPostProcessingEnum.TRANSITION) {
                SetNewCameraProfile(_currentPostProcessingProfile, _previousProfileEnum);
            }
        }

        private void SetNewCameraProfile(PostProcessingProfile profile, CameraPostProcessingEnum postProcessingEnum) {
            _previousProfileEnum = _currentProfileEnum;
            _currentProfileEnum = postProcessingEnum;

            switch (postProcessingEnum) {
                case CameraPostProcessingEnum.NORMAL:
                    _postProcessingTransition.StopTransition();
                    ChangeProfile();
                    break;
                case CameraPostProcessingEnum.DARK:
                    _postProcessingTransition.StopTransition();
                    ChangeProfile();
                    break;
                case CameraPostProcessingEnum.DARKEST:
                    _postProcessingTransition.StopTransition();
                    ChangeProfile();
                    break; 
                case CameraPostProcessingEnum.TRANSITION:
                    _postProcessingTransition.StopTransition();
                    ChangeProfile();
                    break;
            }
        }

        private void ChangeProfile() {
            _currentPostProcessingProfile = _allPostProcessingProfiles[(int) _currentProfileEnum];
            _postProcessingTransition.futureProfile = _currentPostProcessingProfile;
            _postProcessingTransition.RunTranstition();
        }
    }
}