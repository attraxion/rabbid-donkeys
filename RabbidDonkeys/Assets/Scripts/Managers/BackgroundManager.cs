﻿using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityEngine.Rendering;
using Zenject;
using GameManager = Managers.GameManager;

public class BackgroundManager : MonoBehaviour {

    public enum Stages {
        Village,
        Forest,
        Yomi,
        Fuji
    }

    Stages currentStage;

    public GameObject[] Ambient;

    void Start() {
        InvokeRepeating("ChangeStage", 10, 10);
    }

    void Update() {
        //Changes in Village
        if (GameManager.Instance.GameLoopInfo.activeMap == GameManager.MapEnum.VILLAGE) {
            if (GameManager.Instance.enemiesKilled >= 1 && GameManager.Instance.enemiesKilled > GameManager.Instance.enemiesTransitioned) {
                Ambient[0].SetActive(false);
                Ambient[1].SetActive(false);
                Ambient[2].SetActive(true);
                Ambient[3].SetActive(false);
                Ambient[4].SetActive(false);
                Ambient[5].SetActive(false);
                Ambient[6].SetActive(false);
                Ambient[7].SetActive(false);
                Ambient[8].SetActive(false);
                Ambient[9].SetActive(false);
                Ambient[10].SetActive(false);
                Ambient[11].SetActive(false);
            } else if (GameManager.Instance.enemiesTransitioned >= 1 && GameManager.Instance.enemiesTransitioned > GameManager.Instance.enemiesKilled) {
                Ambient[0].SetActive(false);
                Ambient[1].SetActive(true);
                Ambient[2].SetActive(false);
                Ambient[3].SetActive(false);
                Ambient[4].SetActive(false);
                Ambient[5].SetActive(false);
                Ambient[6].SetActive(false);
                Ambient[7].SetActive(false);
                Ambient[8].SetActive(false);
                Ambient[9].SetActive(false);
                Ambient[10].SetActive(false);
                Ambient[11].SetActive(false);
            } else {
                Ambient[0].SetActive(true);
                Ambient[1].SetActive(false);
                Ambient[2].SetActive(false);
                Ambient[3].SetActive(false);
                Ambient[4].SetActive(false);
                Ambient[5].SetActive(false);
                Ambient[6].SetActive(false);
                Ambient[7].SetActive(false);
                Ambient[8].SetActive(false);
                Ambient[9].SetActive(false);
                Ambient[10].SetActive(false);
                Ambient[11].SetActive(false);
            }
        }

        //Changes in forest
        if (GameManager.Instance.GameLoopInfo.activeMap == GameManager.MapEnum.FOREST) {
            if (GameManager.Instance.enemiesKilled >= 1 && GameManager.Instance.enemiesKilled > GameManager.Instance.enemiesTransitioned) {
                Ambient[0].SetActive(false);
                Ambient[1].SetActive(false);
                Ambient[2].SetActive(false);
                Ambient[3].SetActive(false);
                Ambient[4].SetActive(false);
                Ambient[5].SetActive(true);
                Ambient[6].SetActive(false);
                Ambient[7].SetActive(false);
                Ambient[8].SetActive(false);
                Ambient[9].SetActive(false);
                Ambient[10].SetActive(false);
                Ambient[11].SetActive(false);
            } else if (GameManager.Instance.enemiesTransitioned >= 1 && GameManager.Instance.enemiesTransitioned > GameManager.Instance.enemiesKilled) {
                Ambient[0].SetActive(false);
                Ambient[1].SetActive(false);
                Ambient[2].SetActive(false);
                Ambient[3].SetActive(false);
                Ambient[4].SetActive(true);
                Ambient[5].SetActive(false);
                Ambient[6].SetActive(false);
                Ambient[7].SetActive(false);
                Ambient[8].SetActive(false);
                Ambient[9].SetActive(false);
                Ambient[10].SetActive(false);
                Ambient[11].SetActive(false);
            } else {
                Ambient[0].SetActive(false);
                Ambient[1].SetActive(false);
                Ambient[2].SetActive(false);
                Ambient[3].SetActive(true);
                Ambient[4].SetActive(false);
                Ambient[5].SetActive(false);
                Ambient[6].SetActive(false);
                Ambient[7].SetActive(false);
                Ambient[8].SetActive(false);
                Ambient[9].SetActive(false);
                Ambient[10].SetActive(false);
                Ambient[11].SetActive(false);
            }
        }

        //Changes in Yomi
        if (GameManager.Instance.GameLoopInfo.activeMap == GameManager.MapEnum.YOMI) {
            if (GameManager.Instance.enemiesKilled >= 1 && GameManager.Instance.enemiesKilled > GameManager.Instance.enemiesTransitioned) {
                Ambient[0].SetActive(false);
                Ambient[1].SetActive(false);
                Ambient[2].SetActive(false);
                Ambient[3].SetActive(false);
                Ambient[4].SetActive(false);
                Ambient[5].SetActive(false);
                Ambient[6].SetActive(false);
                Ambient[7].SetActive(false);
                Ambient[8].SetActive(true);
                Ambient[9].SetActive(false);
                Ambient[10].SetActive(false);
                Ambient[11].SetActive(false);
            } else if (GameManager.Instance.enemiesTransitioned >= 1 && GameManager.Instance.enemiesTransitioned > GameManager.Instance.enemiesKilled) {
                Ambient[0].SetActive(false);
                Ambient[1].SetActive(false);
                Ambient[2].SetActive(false);
                Ambient[3].SetActive(false);
                Ambient[4].SetActive(false);
                Ambient[5].SetActive(false);
                Ambient[6].SetActive(false);
                Ambient[7].SetActive(true);
                Ambient[8].SetActive(false);
                Ambient[9].SetActive(false);
                Ambient[10].SetActive(false);
                Ambient[11].SetActive(false);
            } else {
                Ambient[0].SetActive(false);
                Ambient[1].SetActive(false);
                Ambient[2].SetActive(false);
                Ambient[3].SetActive(false);
                Ambient[4].SetActive(false);
                Ambient[5].SetActive(false);
                Ambient[6].SetActive(true);
                Ambient[7].SetActive(false);
                Ambient[8].SetActive(false);
                Ambient[9].SetActive(false);
                Ambient[10].SetActive(false);
                Ambient[11].SetActive(false);
            }
        }

        //Changes in Fuji
        if (GameManager.Instance.GameLoopInfo.activeMap == GameManager.MapEnum.FUJI) {
            if (GameManager.Instance.enemiesKilled >= 1 && GameManager.Instance.enemiesKilled > GameManager.Instance.enemiesTransitioned) {
                Ambient[0].SetActive(false);
                Ambient[1].SetActive(false);
                Ambient[2].SetActive(false);
                Ambient[3].SetActive(false);
                Ambient[4].SetActive(false);
                Ambient[5].SetActive(false);
                Ambient[6].SetActive(false);
                Ambient[7].SetActive(false);
                Ambient[8].SetActive(false);
                Ambient[9].SetActive(false);
                Ambient[10].SetActive(false);
                Ambient[11].SetActive(true);
            } else if (GameManager.Instance.enemiesTransitioned >= 1 && GameManager.Instance.enemiesTransitioned > GameManager.Instance.enemiesKilled) {
                Ambient[0].SetActive(false);
                Ambient[1].SetActive(false);
                Ambient[2].SetActive(false);
                Ambient[3].SetActive(false);
                Ambient[4].SetActive(false);
                Ambient[5].SetActive(false);
                Ambient[6].SetActive(false);
                Ambient[7].SetActive(false);
                Ambient[8].SetActive(false);
                Ambient[9].SetActive(false);
                Ambient[10].SetActive(true);
                Ambient[11].SetActive(false);
            } else {
                Ambient[0].SetActive(false);
                Ambient[1].SetActive(false);
                Ambient[2].SetActive(false);
                Ambient[3].SetActive(false);
                Ambient[4].SetActive(false);
                Ambient[5].SetActive(false);
                Ambient[6].SetActive(false);
                Ambient[7].SetActive(false);
                Ambient[8].SetActive(false);
                Ambient[9].SetActive(true);
                Ambient[10].SetActive(false);
                Ambient[11].SetActive(false);
            }
        }
    }

    private void ActivateOneAmbient(int ambientIndex) {
        
    }

    void ChangeStage() {
        if (currentStage != Stages.Fuji)
            currentStage += 1;
        else if (currentStage == Stages.Fuji)
            currentStage = Stages.Village;
        Debug.Log("Changed");
    }

}