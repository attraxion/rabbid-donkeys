﻿using UnityEngine;

namespace Managers {

    public class InputManager : MonoBehaviour {
        public bool tap, swipeLeft, swipeRight, swipeUp, swipeDown;
        public bool isHolding;
        public Vector2 startTouch, swipeDelta;

        private Player.Player _player;

        private void Start() {
            _player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player.Player>();
        }

        private void Update() {
            if (Input.GetKeyDown(KeyCode.F)) {
                _player.OnAttackInputDown();
            }

            if (Input.GetKeyDown(KeyCode.Space)) { }
            if (Input.GetKeyUp(KeyCode.Space)) { }

            tap = swipeLeft = swipeRight = swipeUp = swipeDown = false;

            //InputsMouse
            if (Input.GetMouseButtonDown(0)) {
                tap = true;
                isHolding = true;
                startTouch = Input.mousePosition;
            } else if (Input.GetMouseButtonUp(0)) {
                isHolding = false;
                Reset();
            }

            //MobileInputs
            if (Input.touches.Length > 0) {
                if (Input.touches[0].phase == TouchPhase.Began) {
                    tap = true;
                    isHolding = true;
                    startTouch = Input.touches[0].position;
                } else if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled) {
                    isHolding = false;
                    Reset();
                }
            }

            //Calculate distance
            swipeDelta = Vector2.zero;
            if (isHolding) {
                if (Input.touches.Length > 0)
                    swipeDelta = Input.touches[0].position - startTouch;
                else if (Input.GetMouseButton(0))
                    swipeDelta = (Vector2) Input.mousePosition - startTouch;
            }

            //Recognizing the swipe
            if (swipeDelta.magnitude > 125) {
                //Which direction?
                float x = swipeDelta.x;
                float y = swipeDelta.y;
                if (Mathf.Abs(x) > Mathf.Abs(y)) {
                    //Left or Right
                    if (x < 0)
                        swipeLeft = true;
                    else
                        swipeRight = true;
                } else {
                    //Up or Down
                    if (y < 0)
                        swipeDown = true;
                    else
                        swipeUp = true;
                }

                Reset();
            }
        }

        private void Reset() {
            startTouch = swipeDelta = Vector2.zero;
            isHolding = false;
        }
    }
}