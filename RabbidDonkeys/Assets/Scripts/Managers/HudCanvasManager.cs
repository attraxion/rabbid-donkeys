﻿using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

namespace Managers {

    public class HudCanvasManager : MonoBehaviour {

        [SerializeField] private GameObject _doomedContainer;
        [SerializeField] private Text _scoreText;

        private static HudCanvasManager _instance;
        public static HudCanvasManager Instance { get { return _instance; } }

        private void Awake() {
            if (_instance == null) {
                _instance = this;
            }
        }

        private void Start() {
            _doomedContainer.SetActive(false);
        }

        public void DoomedContainerActivate() {
            _doomedContainer.SetActive(true);
        }

        public void DoomedContainerDeactivate() {
            _doomedContainer.SetActive(false);
        }

        public void UpdateHudScore(float score) {
            if (!_scoreText.gameObject.activeSelf) {
                _scoreText.gameObject.SetActive(true);
            }
            _scoreText.text = score.ToString(CultureInfo.InvariantCulture);
        }

        public void ResetHudScore() {
            _scoreText.text = "0";
        }

    }
}