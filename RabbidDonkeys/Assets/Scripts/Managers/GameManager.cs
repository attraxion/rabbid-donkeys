﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Resources;
using System.Runtime.InteropServices;
using DG.Tweening;
using JetBrains.Annotations;
using Player;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Player = Player.Player;
using Random = UnityEngine.Random;

namespace Managers{
    public class GameManager : MonoBehaviour{
        public struct GameLoopInfoStruct{
            public MapEnum activeMap;
            public int loopIndex;
            public int thresholdIndex;
            public float scoreNeededToChangeMap;
        }

        public enum MapEnum{
            VILLAGE,
            FOREST,
            YOMI,
            FUJI
        }

        private static GameManager _instance;

        public static GameManager Instance {
            get { return _instance; }
            set { _instance = value; }
        }

        public GameLoopInfoStruct GameLoopInfo { get; private set; }

        [SerializeField] private string _mainSceneName;
        [SerializeField] private GameObject _doomedContainer;
        [CanBeNull] [SerializeField] private GameObject _playGameContainer;

        public float score;

        public int enemiesKilled = 0;
        public int enemiesTransitioned = 0;

        private DOTweenAnimation[] doTweenAnimations;
        [SerializeField] private float _secondPoinsAmount;
        [SerializeField] private float _nextScoreBreakpoint;

        public GameObject VillageGO;
        public GameObject ForestGO;
        public GameObject YomiGO;
        public GameObject FujiGO;

        private void Awake(){
            Time.timeScale = 1;
            if (_instance == null) {
                _instance = this;
            }
            DontDestroyOnLoad(gameObject);
            if (doTweenAnimations == null && _playGameContainer != null) {
                doTweenAnimations = _playGameContainer.GetComponentsInChildren<DOTweenAnimation>();
            }
            if (SceneManager.GetActiveScene().name == _mainSceneName) {
                GameLoopInfo = new GameLoopInfoStruct {
                    activeMap = MapEnum.VILLAGE,
                    loopIndex = 0,
                    thresholdIndex = 0,
                    scoreNeededToChangeMap = 100f
                };
            }

            //get all maps
        }

        private void Start(){
            gameObject.SetActive(true);
            _doomedContainer.SetActive(false);
        }

        private void Update(){
            if (Time.timeScale == 0) {
                Time.timeScale = 1;
            }
            if (PlayerStateManager.Instance != null) {
                if (PlayerStateManager.Instance.currentPlayerState == PlayerState.DEAD) {
                    Time.timeScale = 0;
                    score = 0;
                    HudCanvasManager.Instance.ResetHudScore();
                    HudCanvasManager.Instance.DoomedContainerActivate();
                    if (Input.GetKeyDown(KeyCode.Escape)) {
                        HudCanvasManager.Instance.DoomedContainerDeactivate();
                        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);
                    }
                }
            }
            if (score >= GameLoopInfo.scoreNeededToChangeMap) {
                var mapBeforeChange = GameLoopInfo.activeMap;
                SetActiveMap(GetNextMap());
                GameLoopInfo = new GameLoopInfoStruct {
                    activeMap = GameLoopInfo.activeMap,
                    loopIndex = GameLoopInfo.loopIndex,
                    scoreNeededToChangeMap = GameLoopInfo.scoreNeededToChangeMap + _nextScoreBreakpoint,
                    thresholdIndex = GameLoopInfo.thresholdIndex
                };
                ManageMapGameObjects(mapBeforeChange);
                _nextScoreBreakpoint = _nextScoreBreakpoint + Random.Range(100f, 200f);
            }
        }

        private MapEnum GetNextMap(){
            switch (GameLoopInfo.activeMap) {
                case MapEnum.VILLAGE: {
                    return MapEnum.FOREST;
                }
                case MapEnum.FOREST: {
                    return MapEnum.YOMI;
                }
                case MapEnum.YOMI: {
                    return MapEnum.FUJI;
                }
                case MapEnum.FUJI: {
                    return MapEnum.VILLAGE;
                }
                default:
                    return MapEnum.VILLAGE;
            }
        }

        public void PlayNewGameInvoke(){
            foreach (DOTweenAnimation doTweenAnimation in doTweenAnimations) {
                if (!doTweenAnimation.isFrom) {
                    doTweenAnimation.DOPlay();
                }
            }
            StartCoroutine(PlayNewGameCoroutine());
        }

        private IEnumerator PlayNewGameCoroutine(){
            yield return new WaitForSeconds(2f);
            SceneManager.LoadSceneAsync(_mainSceneName);
            InvokeRepeating("AddScoreEverySecond", 0.1f, 0.1f);
            FindAllMapGameObjects();
        }

        private void AddScoreEverySecond(){
            score += _secondPoinsAmount;
            HudCanvasManager.Instance.UpdateHudScore(score);
        }

        //TODO: EVERY THRESHOLD AND LEVEL SHOULD CHANGE LevelDesigner procedural map and we should have mechanism that will adjust difficulty, speed, level maps

        private void SetActiveMap(MapEnum newActiveMap){
            if (SceneManager.GetActiveScene().name == _mainSceneName) {
                GameLoopInfo = new GameLoopInfoStruct {
                    activeMap = newActiveMap,
                    loopIndex = GameLoopInfo.loopIndex,
                    thresholdIndex = GameLoopInfo.thresholdIndex
                };
            }
        }

        private void FindAllMapGameObjects(){
            VillageGO = GameObject.Find("VILLAGE");
            ForestGO = GameObject.Find("FOREST");
//            YomiGO = GameObject.Find("Yomi");
//            FujiGO = GameObject.Find("Fuji");
        }

        private void ManageMapGameObjects(MapEnum mapBeforeChange){
            if (mapBeforeChange != GameLoopInfo.activeMap) {
                switch (mapBeforeChange.ToString()) {
                    case "VILLAGE": {
                        Debug.Log("Change to forest from village");
                        VillageGO.SetActive(false);
                        ForestGO.SetActive(true);
                        break;
                    }
                }
            }
        }
    }
}