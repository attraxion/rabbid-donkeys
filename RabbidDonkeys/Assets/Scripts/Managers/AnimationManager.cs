﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class AnimationManager : MonoBehaviour
{
	private Animator anim;	
	
	void Start ()
	{
		anim = GetComponent<Animator>();
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.layer == 18)
		{
			anim.SetBool("Wake", true);
		}

		if (other.gameObject.layer == 19)
		{			
			anim.SetBool("Attack", true);
		}
	}
}