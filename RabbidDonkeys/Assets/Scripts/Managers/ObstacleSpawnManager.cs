﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameManager = Managers.GameManager;

public class ObstacleSpawnManager : MonoBehaviour {

    public GameObject[] VillageE;
    public GameObject[] ForestE;
    public GameObject[] YomiE;
    public GameObject[] FujiE;

    public Transform[] Spawnpoint;

    private int randomObstacle;

    void Start() {
        if (GameManager.Instance.GameLoopInfo.activeMap == GameManager.MapEnum.VILLAGE) {
            randomObstacle = Random.Range(0, 2);
            Instantiate(VillageE[randomObstacle], Spawnpoint[0].position, Spawnpoint[0].rotation);
        } else if (GameManager.Instance.GameLoopInfo.activeMap == GameManager.MapEnum.FOREST) {
            randomObstacle = Random.Range(3, 6);
            Instantiate(ForestE[randomObstacle], Spawnpoint[0].position, Spawnpoint[0].rotation);
        } else if (GameManager.Instance.GameLoopInfo.activeMap == GameManager.MapEnum.YOMI) {
            randomObstacle = Random.Range(7, 9);
            Instantiate(YomiE[randomObstacle], Spawnpoint[0].position, Spawnpoint[0].rotation);
        } else if (GameManager.Instance.GameLoopInfo.activeMap == GameManager.MapEnum.FUJI) {
            randomObstacle = Random.Range(10, 12);
            Instantiate(FujiE[randomObstacle], Spawnpoint[0].position, Spawnpoint[0].rotation);
        }
    }
}