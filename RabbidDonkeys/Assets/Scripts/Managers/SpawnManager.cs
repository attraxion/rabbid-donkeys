﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameManager = Managers.GameManager;

public class SpawnManager : MonoBehaviour {

	public GameObject[] VillageE;
	public GameObject[] ForestE;
	public GameObject[] YomiE;
	public GameObject[] FujiE;
	
	public Transform[] Spawnpoint;

	private int randomEnemy;
	
	void Start ()
	{
		if (GameManager.Instance.GameLoopInfo.activeMap == GameManager.MapEnum.VILLAGE)
		{
			randomEnemy = Random.Range(0, 2);
			Instantiate(VillageE[randomEnemy], Spawnpoint[0].position, Spawnpoint[0].rotation);
		}
		else if (GameManager.Instance.GameLoopInfo.activeMap == GameManager.MapEnum.FOREST)
		{
			randomEnemy = Random.Range(3, 6);
			Instantiate(ForestE[randomEnemy], Spawnpoint[0].position, Spawnpoint[0].rotation);
		}
		else if (GameManager.Instance.GameLoopInfo.activeMap == GameManager.MapEnum.YOMI)
		{
			randomEnemy = Random.Range(7, 9);
			Instantiate(YomiE[randomEnemy], Spawnpoint[0].position, Spawnpoint[0].rotation);
		}
		else if (GameManager.Instance.GameLoopInfo.activeMap == GameManager.MapEnum.FUJI)
		{
			randomEnemy = Random.Range(10, 12);
			Instantiate(FujiE[randomEnemy], Spawnpoint[0].position, Spawnpoint[0].rotation);
		}
		
	}
}
