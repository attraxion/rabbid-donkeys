﻿using UnityEngine;

public class AttackTrigger : MonoBehaviour{
    void OnTriggerEnter(Collider other){
        if (other.gameObject.layer == 12) {
            Debug.Log("TRIGGERED ENEMY");
            Destroy(other.gameObject);
        }
    }
}