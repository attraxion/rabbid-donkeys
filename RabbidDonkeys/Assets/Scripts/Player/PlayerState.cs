﻿namespace Player {
    public enum PlayerState{
        NONE,
        NORMAL,
        TRANSITION,
        ATTACKING,
        HIT,
        DEAD,
        RESURRECTABLE
    }
}