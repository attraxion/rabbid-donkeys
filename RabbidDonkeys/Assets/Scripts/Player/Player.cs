﻿using System.Collections;
using Managers;
using UnityEngine;
using GameManager = Managers.GameManager;

namespace Player {
    [RequireComponent(typeof(PlayerMovementController))]
    [RequireComponent(typeof(SkinnedMeshRenderer))]
    public class Player : MonoBehaviour {
        public GameObject fPoint, bPoint;

        //player is singleton
        private static Player _instance;

        public static Player Instance { get { return _instance; } }

        //movement variables
        public float moveSpeed = 10f;

        public float maxJumpHeight = 4f;
        public float minjumpHeight = 1f;
        public float timeToJumpApex = 0.4f;
        private float _accelerationTimeAirborne = .2f;
        private float _accelerationTimeGrounded = .1f;
        private float _gravity;
        private float _maxJumpVelocity;
        private float _minJumpVelocity;
        private Vector3 _velocity;
        private float _velocityXSmoothing;
        private Vector3 _moveVector;
        private float _verticalVelocity;

        private PlayerMovementController _controller; //currently not necessary
        private CharacterController _characterController;

        private Vector3 _directionalInput = Vector3.right;

        [SerializeField] private float _prevTransitionFormUseTime;
        [SerializeField] private float _transitionFormCooldown = 2f;

        public bool _grounded; //is player grounded - cannot double jump

        //physics variables
        private Rigidbody _rigidbody;

        private RaycastHit _hit;

        //player state vars
        private const PlayerState _defaultPlayerState = PlayerState.NORMAL;

        private PlayerStateManager _playerStateManager;
        public LayerMask collisionMask;

        private PlayerAnimatorManager _playerAnimatorManager;

        [SerializeField] private SkinnedMeshRenderer _skinnedMeshRenderer;

        private GroundChecker _groundChecker;

        //transitioning/phasing vars
        private CapsuleCollider _hitCollider;

        private TransitionThrough _transitionThrough;
        [SerializeField] private float _timeToTurnOffTransitionForm = .25f;
        [SerializeField] private float _dashMultiplier;
        private bool _currentlyInTransitionMode = false;

        private void Awake() {
            if (_instance == null) {
                _instance = this;
            }
        }

        private void Start() {
            InitPlayer();
        }

        private void Update() {
            HandlePlayerMovement();
            HandleTransitionForm();
//            RotateToStickGround();
        }

        private void FixedUpdate() { }

        private void RotateToStickGround() {
            transform.rotation = Quaternion.Slerp(transform.rotation,
                Quaternion.AngleAxis(_groundChecker.groundSlopeAngle, Vector3.forward),
                Time.deltaTime * 3f);
        }

        private void HandlePlayerMovement() {
            _moveVector = Vector3.zero;

            if (_characterController.isGrounded) {
                _verticalVelocity = -0.5f;
            } else {
                _verticalVelocity += _gravity * Time.deltaTime;
            }
            if (_characterController.isGrounded && Input.GetKeyDown(KeyCode.Space)) {
                _playerAnimatorManager.SetTrigger(_playerAnimatorManager.Jump);
                _verticalVelocity = _maxJumpVelocity;
            }
            _moveVector.x = moveSpeed;
            _moveVector.y = _verticalVelocity;
            _characterController.Move(_moveVector * Time.deltaTime);
        }

        public void OnAttackInputDown() {
            _playerAnimatorManager.SetTrigger(_playerAnimatorManager.Attack);
        }

//        private void OnTriggerEnter(Collider other) {
//            if ((other.gameObject.layer == 11 || other.gameObject.layer == 12 ||
//                 other.gameObject.layer == 13) && _playerStateManager.currentPlayerState != PlayerState.TRANSITION) {
//                HandlePlayerDeath();
//            }
//        }

        public void OnJumpInputUp() {
            if (_velocity.y > _minJumpVelocity) {
                _velocity.y = _minJumpVelocity;
            }
        }

        private void HandleTransitionForm() {
            if (Input.GetKeyDown(KeyCode.E) && !_currentlyInTransitionMode) {
                _currentlyInTransitionMode = true;
                PlayerStateManager.Instance.SetState(PlayerState.TRANSITION);
                _prevTransitionFormUseTime = Time.time;
                gameObject.layer = 10;
                _hitCollider.gameObject.layer = 10;
                _transitionThrough.transitionColliderEnabled = true;
                float originalMoveSpeed = moveSpeed;
                moveSpeed = moveSpeed * _dashMultiplier;
                StartCoroutine(MeasureTransitionTimeCoroutine(_timeToTurnOffTransitionForm, originalMoveSpeed));
            }
        }

        private void OnCollisionEnter(Collision other) {
            if (other.collider.gameObject.layer == 11) {
                HandlePlayerDeath();
            }
            if ((other.collider.gameObject.layer == 12 || other.collider.gameObject.layer == 13) &&
                _playerStateManager.currentPlayerState != PlayerState.TRANSITION) {
                HandlePlayerDeath();
            }
            if (_playerStateManager.currentPlayerState == PlayerState.TRANSITION &&
                other.collider.gameObject.layer == 12) {
                Debug.Log("Phased through");
                GameManager.Instance.enemiesTransitioned += 1;
            }
        }

        private IEnumerator MeasureTransitionTimeCoroutine(float time, float originalMoveSpeed) {
            yield return new WaitForSeconds(time);
            if (_transitionThrough.transitionColliderEnabled) {
                TransitionFormTurnOff(originalMoveSpeed);
            }
            yield return new WaitForSeconds(time);
            _currentlyInTransitionMode = false;
        }

        private void TransitionFormTurnOff(float originalMoveSpeed) {
            _transitionThrough.transitionColliderEnabled = false;
            moveSpeed = originalMoveSpeed;
            PlayerStateManager.Instance.SetState(_defaultPlayerState);
            gameObject.layer = 15;
            _hitCollider.gameObject.layer = 15;
        }

        private void HandlePlayerDeath() {
            PlayerStateManager.Instance.SetState(PlayerState.DEAD);
            _playerAnimatorManager.SetTrigger(_playerAnimatorManager.Death);
        }

        private void InitPlayer() {
//            _rigidbody = GetComponent<Rigidbody>();
            _characterController = GetComponent<CharacterController>();
            _playerAnimatorManager = GetComponent<PlayerAnimatorManager>();
            _playerStateManager = GetComponent<PlayerStateManager>();
//            if (_rigidbody == null) {
//                _rigidbody = gameObject.AddComponent<Rigidbody>();
//            }
            _hit = new RaycastHit();
            _controller = GetComponent<PlayerMovementController>();
            _gravity = -(2 * maxJumpHeight) / Mathf.Pow(timeToJumpApex, 2);
            _maxJumpVelocity = Mathf.Abs(_gravity) * timeToJumpApex;
            _minJumpVelocity = Mathf.Sqrt(2 * Mathf.Abs(_gravity) * minjumpHeight);
            _hitCollider = GetComponentInChildren<CapsuleCollider>();
            _transitionThrough = GetComponentInChildren<TransitionThrough>();
            _groundChecker = GetComponent<GroundChecker>();
        }
    }
}