﻿using Player;
using UnityEngine;

namespace Player {

    public class PlayerAttack : MonoBehaviour {
        private bool attacking = false;

        private float attackTimer = 0;
        private const float attackCd = 0.3f;

        public BoxCollider attackTrigger;

        private void Awake() {
            attackTrigger = GetComponentInChildren<BoxCollider>();
            attackTrigger.enabled = false;
        }

        private void Update() {
            if (Input.GetKeyDown(KeyCode.F) && PlayerStateManager.Instance.currentPlayerState != PlayerState.ATTACKING) {
                PlayerStateManager.Instance.SetState(PlayerState.ATTACKING);
                attackTimer = attackCd;
                attackTrigger.enabled = true;
            }

            if (PlayerStateManager.Instance.currentPlayerState != PlayerState.ATTACKING)
                return;
            if (attackTimer > 0) {
                attackTimer -= Time.deltaTime;
            } else {
                PlayerStateManager.Instance.SetState(PlayerStateManager.Instance.previousPlayerState);
                attackTrigger.enabled = false;
            }
        }

    }
}