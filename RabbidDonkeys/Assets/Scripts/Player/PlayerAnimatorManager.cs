﻿using UnityEngine;

namespace Player{
    public class PlayerAnimatorManager : MonoBehaviour{
        public readonly string Run = "Run";
        public readonly string Jump = "Jump";
        public readonly string Attack = "Attack";
        public readonly string Death = "Death";

        private Animator _animator;

        private void Awake(){
            _animator = GetComponent<Animator>();
        }

        public void SetTrigger(string triggerName){
            _animator.SetTrigger(triggerName);
        }

        public void SetBool(string boolName){
            _animator.SetBool(boolName, true);
        }

        public void UnSetBool(string boolName){
            _animator.SetBool(boolName, false);
        }

        // Use this for initialization
        void Start(){
        }

        // Update is called once per frame
        void Update(){
        }
    }
}