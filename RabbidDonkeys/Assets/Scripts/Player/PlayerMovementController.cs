﻿using UnityEngine;

namespace Player {

    public class PlayerMovementController : RaycastController {

        public float maxSlopeAngle = 80;

        private Player _player;

        [HideInInspector] public Vector3 playerInput;

        public override void Start() {
            base.Start();
            _player = GetComponent<Player>();
        }

        public void Move(Vector3 moveAmount, bool standingOnPlatform = false) {
            Move(moveAmount, Vector3.zero, standingOnPlatform);
        }

        public void Move(Vector3 moveAmount, Vector3 input, bool standingOnPlatform = false) {
        }

        private void CheckIfGrounded() {
            float rayLength = 30f;
            Vector3 rayOrigin = transform.position + new Vector3(0f, 0.4f, 0f);

            RaycastHit hit;

            Debug.DrawRay(rayOrigin, Vector3.down, Color.red);
            Physics.Raycast(rayOrigin, Vector3.down, out hit, rayLength, collisionMask);

            bool hitGround = hit.collider != null && hit.collider.gameObject.layer == LayerMask.NameToLayer("Ground");
            _player._grounded = hitGround;
        }

    }

}