﻿using Managers.Interfaces;
using UnityEngine;

namespace Player {
    public class PlayerStateManager : MonoBehaviour, IStateManager<PlayerState> {

        private static PlayerStateManager _instance;
        public static PlayerStateManager Instance { get { return _instance; } }

        public PlayerState currentPlayerState;
        public PlayerState previousPlayerState;

        private void Awake() {
            if (_instance == null) {
                _instance = this;
            }
        }

        private void Start() {
            currentPlayerState = PlayerState.NORMAL;
        }

        public void SetState(PlayerState newState) {
            previousPlayerState = currentPlayerState;
            currentPlayerState = newState;
        }

        public PlayerState GetCurrentState() {
            return currentPlayerState;
        }
    }
}