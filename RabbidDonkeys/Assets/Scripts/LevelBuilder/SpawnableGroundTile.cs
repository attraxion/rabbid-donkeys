﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utils;

//TODO: Remeber to change every instantiate to POOLING!!

namespace LevelBuilder{
    public class SpawnableGroundTile : GroundTile, ISpawnableTile{
        public GameObject thisTileObstacle;
        public GameObject thisTileMainDetail;
        [SerializeField] private bool _disableSpawningOnThisTile;
        [SerializeField] private List<Transform> _obstaclePositions;
        [SerializeField] private List<Transform> _enemyPositions;

        private bool thisTileSpawnEnemy;

        private void Awake(){
            base.Awake();
            _obstaclePositions = gameObject.GetComponentsInChildren<Transform>().ToList()
                .FindAll(p => p.CompareTag(obstacleTagString));
            _enemyPositions = gameObject.GetComponentsInChildren<Transform>().ToList()
                .FindAll(p => p.CompareTag(enemyTagString));
        }

        private void Start(){
            base.Start();
            if (!_disableSpawningOnThisTile) {
                thisTileSpawnEnemy = Random.Range(0, 100) >= 50;
                bool x = Random.Range(0, 100) >= LevelDesigner.Instance.obstacleSpawnPercentage &&
                         LevelDesigner.Instance.canSpawnObstacles &&
                         !thisTileSpawnEnemy;
                if (Random.Range(0, 100) >= LevelDesigner.Instance.obstacleSpawnPercentage &&
                    LevelDesigner.Instance.canSpawnObstacles && !thisTileSpawnEnemy) {
                    PlaceObstacle(RandomUtils.GetRandomListElement(LevelDesigner.Instance.obstacles));
                }
                if (Random.Range(0, 100) >= LevelDesigner.Instance.mainDetailsSpawnPercentage) {
                    PlaceMainDetail(RandomUtils.GetRandomListElement(LevelDesigner.Instance.mainDetails));
                }
                if (Random.Range(0, 100) >= LevelDesigner.Instance.enemySpawnPercentage &&
                    LevelDesigner.Instance.canSpawnEnemies && thisTileSpawnEnemy) {
                    PlaceEnemy(RandomUtils.GetRandomListElement(LevelDesigner.Instance.enemies));
                }
            }
        }

        public void PlaceObstacle(GameObject obstacle){
            Vector3 obstaclePosition = _obstaclePositions[Random.Range(0, _obstaclePositions.Count)].transform.position;
            thisTileObstacle = Instantiate(obstacle, obstaclePosition, obstacle.transform.rotation);
        }

        public void PlaceEnemy(GameObject enemy){
            Vector3 obstaclePosition = _enemyPositions[Random.Range(0, _enemyPositions.Count)].transform.position;
            thisTileObstacle = Instantiate(enemy, obstaclePosition, enemy.transform.rotation);
        }

        private void PlaceMainDetail(GameObject mainDetail){
            Vector3 addBasedOnSize =
                new Vector3(Random.Range(-3, 3f), mainDetail.GetComponent<Renderer>().bounds.size.y, 1f);
            thisTileMainDetail = Instantiate(mainDetail, _meshRenderer.bounds.center + addBasedOnSize,
                mainDetail.transform.rotation);
        }

        private Vector3 CalculateObstaclePositionOnTile(GameObject obstacle){
            Renderer obstacleRenderer = obstacle.GetComponent<Renderer>();
            float boundSizeY = obstacleRenderer == null ? 0f : obstacleRenderer.bounds.size.y;
            return new Vector3(transform.position.x, transform.position.y + tileSize.y / 2f + boundSizeY,
                transform.position.z);
        }
    }
}