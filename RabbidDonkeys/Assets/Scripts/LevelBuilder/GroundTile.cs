﻿using UnityEngine;
using Utils;

namespace LevelBuilder {

    public class GroundTile : MonoBehaviour {

        public int UniqueId;
        public GroundTileType groundTileType;
        protected MeshRenderer _meshRenderer;
        public Vector3 tileSize;
        public Vector3 tileExtents;
        [SerializeField] private Material _currentMaterial;
        [SerializeField] protected bool isVisibleByCamera;
        protected readonly string obstacleTagString = "ObstacleSpawn";
        protected readonly string enemyTagString = "EnemySpawn";
        
        public GameObject anObject;
        public Collider anObjCollider;
        private Camera cam;
        private Plane[] planes;

        protected void Awake() {
            _meshRenderer = GetComponent<MeshRenderer>();
            if (_meshRenderer == null) {
                GetComponentInChildren<MeshRenderer>();
            }
            tileSize = _meshRenderer.bounds.size;
            tileExtents = _meshRenderer.bounds.extents;
            if (_currentMaterial == null) {
                _currentMaterial = _meshRenderer.material;
            }
        }

        protected void Start() {
            if (UniqueId == 0) {
                UniqueId = SetUniqueId();
            }
        }

        private void Update() {
//            if (!GeometryUtility.TestPlanesAABB(planes, anObjCollider.bounds)) {
//                Debug.Log(gameObject.name + " not in camera view and behind player!");
//                Destroy(gameObject);
//            }
        }

        private int SetUniqueId() {
            int id = LevelDesigner.Instance.lastVisiblePlatformId += 1;
            return id;
        }
    }
}