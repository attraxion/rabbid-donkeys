﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace LevelBuilder {
    public static class PlatformPooler {
        public static List<GameObject> GeneratePlatformsPool(IEnumerable<GameObject> platforms) {
            List<GameObject> pool = new List<GameObject>();
            foreach (var platform in platforms) {
                SpawnableGroundTile spawnableGroundTile = platform.GetComponent<SpawnableGroundTile>();
                GroundTileType groundTileType = spawnableGroundTile.groundTileType;

                int numberOfPlaftormsForThisType;

                switch (groundTileType) {
                    case GroundTileType.TWO_TWO:
                        numberOfPlaftormsForThisType = Random.Range(1, 6);
                        break;
                    case GroundTileType.TWO_FOUR:
                        numberOfPlaftormsForThisType = Random.Range(1, 12);
                        break;
                    case GroundTileType.TWO_SIX:
                        numberOfPlaftormsForThisType = Random.Range(1, 23);
                        break;
                    case GroundTileType.FOUR_TWO:
                        numberOfPlaftormsForThisType = Random.Range(1, 14);
                        break;
                    case GroundTileType.FOUR_FOUR:
                        numberOfPlaftormsForThisType = Random.Range(1, 14);
                        break;
                    case GroundTileType.FOUR_SIX:
                        numberOfPlaftormsForThisType = Random.Range(1, 9);
                        break;
                    case GroundTileType.SIX_TWO:
                        numberOfPlaftormsForThisType = Random.Range(1, 4);
                        break;
                    case GroundTileType.SIX_FOUR:
                        numberOfPlaftormsForThisType = Random.Range(1, 9);
                        break;
                    case GroundTileType.SIX_SIX:
                        numberOfPlaftormsForThisType = Random.Range(1, 8);
                        break;
                    case GroundTileType.TEN_TWO:
                        numberOfPlaftormsForThisType = Random.Range(1, 5);
                        break;
                    case GroundTileType.TEN_FOUR:
                        numberOfPlaftormsForThisType = Random.Range(1, 7);
                        break;
                    case GroundTileType.TEN_SIX:
                        numberOfPlaftormsForThisType = Random.Range(1, 3);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                for (int i = 0; i < numberOfPlaftormsForThisType; i++) {
                    pool.Add(platform);
                }
            }
            return pool;
        }
    }
}