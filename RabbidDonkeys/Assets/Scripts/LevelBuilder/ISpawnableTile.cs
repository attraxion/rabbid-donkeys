﻿using UnityEngine;

namespace LevelBuilder {
    public interface ISpawnableTile {
        void PlaceObstacle(GameObject obstacle);
        void PlaceEnemy(GameObject enemy);
    }
}