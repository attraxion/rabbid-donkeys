﻿namespace LevelBuilder {
    public enum GroundTileType {
        TWO_TWO,
        TWO_FOUR,
        TWO_SIX,
        FOUR_TWO,
        FOUR_FOUR,
        FOUR_SIX,
        SIX_TWO,
        SIX_FOUR,
        SIX_SIX,
        TEN_TWO,
        TEN_FOUR,
        TEN_SIX
    }
}