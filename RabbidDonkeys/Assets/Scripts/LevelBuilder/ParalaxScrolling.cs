﻿using System.Collections;
using System.Runtime.Remoting.Channels;
using UnityEngine;
using Zenject;
using GameManager = Managers.GameManager;

namespace LevelBuilder{
    public class ParalaxScrolling : MonoBehaviour{
        public float backgroundSize;
        public float paralaxSpeed;

        private Transform cameraTransform;
        private Transform[] layers;
        private float viewZone = -10;
        private int leftIndex;
        private int rightIndex;
        private float lastCameraX;


        private void Start(){
            cameraTransform = Camera.main.transform;
            lastCameraX = cameraTransform.position.x;
            layers = new Transform[transform.childCount];
            for (int i = 0; i < transform.childCount; i++)
                layers[i] = transform.GetChild(i);
            leftIndex = 0;
            rightIndex = layers.Length - 1;
        }

        private void Update(){
            float DeltaX = cameraTransform.position.x - lastCameraX;
            transform.position += Vector3.right * DeltaX * paralaxSpeed;

            lastCameraX = cameraTransform.position.x;

            if (cameraTransform.position.x > (layers[rightIndex].transform.position.x) + viewZone)
                ScrollRight();
        }

        private void ScrollRight(){
            int LastLeft = leftIndex;
            layers[leftIndex].position = new Vector3(1 * (layers[rightIndex].position.x + backgroundSize),
                1 * layers[rightIndex].position.y,
                1 * layers[rightIndex].transform.position.z);
            rightIndex = leftIndex;
            leftIndex++;
            if (leftIndex == layers.Length)
                leftIndex = 0;
        }
    }
}