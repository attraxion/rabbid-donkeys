﻿using System;
using System.Collections.Generic;

namespace LevelBuilder {

    public static class SpawnRules {
        public static List<GroundTileType> RulesForType(GroundTileType rulesForTileType) {
            while (true) {
                int i = 20;
                List<GroundTileType> providedTileTypes = new List<GroundTileType>();
                switch (rulesForTileType) {
                    case GroundTileType.TWO_TWO:
                        providedTileTypes.Add(GroundTileType.TWO_TWO);
                        providedTileTypes.Add(GroundTileType.TWO_FOUR);
                        providedTileTypes.Add(GroundTileType.TWO_SIX);
                        providedTileTypes.Add(GroundTileType.TEN_TWO);
                        return providedTileTypes;
                    case GroundTileType.TWO_FOUR:
                        providedTileTypes.Add(GroundTileType.FOUR_TWO);
                        providedTileTypes.Add(GroundTileType.FOUR_FOUR);
                        providedTileTypes.Add(GroundTileType.FOUR_SIX);
                        providedTileTypes.Add(GroundTileType.TEN_FOUR);
                        return providedTileTypes;
                    case GroundTileType.TWO_SIX:
                        providedTileTypes.Add(GroundTileType.SIX_TWO);
                        providedTileTypes.Add(GroundTileType.SIX_FOUR);
                        providedTileTypes.Add(GroundTileType.SIX_SIX);
                        providedTileTypes.Add(GroundTileType.TEN_SIX);
                        return providedTileTypes;
                    case GroundTileType.FOUR_TWO:
                        providedTileTypes.Add(GroundTileType.TWO_TWO);
                        providedTileTypes.Add(GroundTileType.TWO_FOUR);
                        providedTileTypes.Add(GroundTileType.TWO_SIX);
                        providedTileTypes.Add(GroundTileType.TEN_TWO);
                        return providedTileTypes;
                    case GroundTileType.FOUR_FOUR:
                        providedTileTypes.Add(GroundTileType.FOUR_TWO);
                        providedTileTypes.Add(GroundTileType.FOUR_FOUR);
                        providedTileTypes.Add(GroundTileType.FOUR_SIX);
                        providedTileTypes.Add(GroundTileType.TEN_FOUR);
                        return providedTileTypes;
                    case GroundTileType.FOUR_SIX:
                        providedTileTypes.Add(GroundTileType.SIX_TWO);
                        providedTileTypes.Add(GroundTileType.SIX_FOUR);
                        providedTileTypes.Add(GroundTileType.SIX_SIX);
                        providedTileTypes.Add(GroundTileType.TEN_SIX);
                        return providedTileTypes;
                    case GroundTileType.SIX_TWO:
                        providedTileTypes.Add(GroundTileType.TWO_TWO);
                        providedTileTypes.Add(GroundTileType.TWO_FOUR);
                        providedTileTypes.Add(GroundTileType.TWO_SIX);
                        providedTileTypes.Add(GroundTileType.TEN_TWO);
                        return providedTileTypes;
                    case GroundTileType.SIX_FOUR:
                        providedTileTypes.Add(GroundTileType.FOUR_TWO);
                        providedTileTypes.Add(GroundTileType.FOUR_FOUR);
                        providedTileTypes.Add(GroundTileType.FOUR_SIX);
                        providedTileTypes.Add(GroundTileType.TEN_FOUR);
                        return providedTileTypes;
                    case GroundTileType.SIX_SIX:
                        providedTileTypes.Add(GroundTileType.SIX_TWO);
                        providedTileTypes.Add(GroundTileType.SIX_FOUR);
                        providedTileTypes.Add(GroundTileType.SIX_SIX);
                        providedTileTypes.Add(GroundTileType.TEN_SIX);
                        return providedTileTypes;
                    case GroundTileType.TEN_TWO:
                        providedTileTypes.Add(GroundTileType.TWO_TWO);
                        providedTileTypes.Add(GroundTileType.TWO_FOUR);
                        providedTileTypes.Add(GroundTileType.TWO_SIX);
                        providedTileTypes.Add(GroundTileType.TEN_TWO);
                        return providedTileTypes;
                    case GroundTileType.TEN_FOUR:
                        providedTileTypes.Add(GroundTileType.FOUR_TWO);
                        providedTileTypes.Add(GroundTileType.FOUR_FOUR);
                        providedTileTypes.Add(GroundTileType.FOUR_SIX);
                        providedTileTypes.Add(GroundTileType.TEN_FOUR);
                        return providedTileTypes;
                    case GroundTileType.TEN_SIX:
                        providedTileTypes.Add(GroundTileType.SIX_TWO);
                        providedTileTypes.Add(GroundTileType.SIX_FOUR);
                        providedTileTypes.Add(GroundTileType.SIX_SIX);
                        providedTileTypes.Add(GroundTileType.TEN_SIX);
                        return providedTileTypes;
                    default:
                        //prevent infinite loop
                        i -= 1;
                        if (i == 0)
                            throw new ArgumentOutOfRangeException();
                        continue;
                }
            }
        }
    }
}