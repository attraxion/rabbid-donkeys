﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FujiFollow : MonoBehaviour{
    private Transform cameraTransform;
    private float lastCameraX;


    private void Start(){
        cameraTransform = Camera.main.transform;
        lastCameraX = cameraTransform.position.x;
    }

    private void Update(){
        float DeltaX = cameraTransform.position.x;
        transform.position = Vector3.right * DeltaX;

        lastCameraX = cameraTransform.position.x;
    }
}