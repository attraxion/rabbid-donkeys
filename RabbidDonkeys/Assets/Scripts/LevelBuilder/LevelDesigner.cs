﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using Utils;
using Random = UnityEngine.Random;

namespace LevelBuilder {

    public class LevelDesigner : MonoBehaviour {

        private static LevelDesigner _instance;
        public static LevelDesigner Instance { get { return _instance; } }

        private Camera _mainCamera;

        //all platform currently on scene that player can move
        private Queue<SpawnableGroundTile> _platformsQueue;
        public int lastVisiblePlatformId;

        public float obstacleSpawnPercentage;
        public bool canSpawnObstacles;
        public List<GameObject> obstacles;

        public float enemySpawnPercentage;
        public bool canSpawnEnemies;
        public List<GameObject> enemies;

        public float mainDetailsSpawnPercentage;
        public List<GameObject> mainDetails;

        [SerializeField] private List<GameObject> _platformsList;
        [SerializeField] private List<GameObject> _tempPlatformsPool;
        [SerializeField] private List<GameObject> _finalPlatformsPool;
        [SerializeField] private float _timeFromPrevPlacement;

        private float _spawnTimeRemaining;
        private float _spawnThresholdTime = 6f;

        private bool _shouldRemovePlatform;
        private float _removeTimeRemaining;
        private float _removeThresholdTime = 1f;

        private int platformSpawnSpeed;
        [SerializeField] private float _distBetweenPlayerAndLastTile;
        [SerializeField] private int _spawnedPlatformsAtOneTime;

        //TODO: Do we want to get SpawnableGroundTile first from the left that is visible in camera extents??

        private void Awake() {
            if (_instance == null) {
                _instance = this;
            }
            _tempPlatformsPool =
                PlatformPooler.GeneratePlatformsPool(_platformsList); //Designer should take next platform from this list, put it on scene and add it to queue
            foreach (var platform in _tempPlatformsPool) {
                GameObject instantiated = Instantiate(platform, transform.position, gameObject.transform.rotation, gameObject.transform);
                instantiated.SetActive(false);
                _finalPlatformsPool.Add(instantiated);
            }
        }

        private void Start() {
            _platformsQueue = new Queue<SpawnableGroundTile>(GetSortedVisibleSpawnableGroundTiles(false));
            lastVisiblePlatformId = _platformsQueue.Last().UniqueId;
            _spawnTimeRemaining = _spawnThresholdTime;
            _removeTimeRemaining = _removeThresholdTime;
        }

        private void Update() {
            if (Vector3.Distance(Player.Player.Instance.transform.position,
                    _platformsQueue.ToList().Find(p => p.UniqueId == lastVisiblePlatformId).transform.position) <
                _distBetweenPlayerAndLastTile) {
                for (int i = 0; i < _spawnedPlatformsAtOneTime; i++) {
                    PlaceItOnScene(SelectNextPlatform());
                }
            }
            _shouldRemovePlatform = ItsTimeToRemovePlatform();
            if (_shouldRemovePlatform) {
                CleanNotVisiblePlatforms();
            }
        }

        private bool ItsTimeToRemovePlatform() {
            _spawnTimeRemaining -= Time.deltaTime;
            if (_spawnTimeRemaining > 0) {
                float seconds = Mathf.Floor(_spawnTimeRemaining % 60);

                if (Mathf.Approximately(seconds, 0f)) {
                    _spawnTimeRemaining = _spawnThresholdTime;
                    return true;
                }
                return false;
            }
            return false;
        }

        private void CleanNotVisiblePlatforms() {
            if (_platformsQueue.Any()) {
                SpawnableGroundTile dequeued = _platformsQueue.Dequeue();
                Destroy(dequeued.thisTileObstacle);
                Destroy(dequeued.thisTileMainDetail);
                Destroy(dequeued.gameObject);
            }
        }

        private void PlaceItOnScene(GameObject platformToPlace) {
            SpawnableGroundTile lastPlatformSpawnableGroundTile = _platformsQueue.Last();
            Vector3 lastPlatformPosition = lastPlatformSpawnableGroundTile.transform.position;
            Vector3 lastPlatformExtents = lastPlatformSpawnableGroundTile.tileExtents;

            SpawnableGroundTile platformToPlaceSpawnableGroundTile = platformToPlace.GetComponent<SpawnableGroundTile>();

            Vector3 platformExtents = platformToPlaceSpawnableGroundTile.tileExtents;
            //TODO: Improve placement position..
            Vector3 placementPosition =
                lastPlatformPosition + new Vector3(platformExtents.x + lastPlatformExtents.x, 0, 0);

//            Quaternion platformRotation = platformToPlace.transform.rotation;

            var inst = Instantiate(platformToPlace, placementPosition, Quaternion.identity,
                transform); //TODO: not efficient solution, we should use pooling (later) or never...
            inst.SetActive(true);
            _platformsQueue.Enqueue(inst.GetComponent<SpawnableGroundTile>());
        }

        private GameObject SelectNextPlatform() {
            SpawnableGroundTile currentLastPlatform = _platformsQueue.Last();
            //select according to set of rules..
            List<GroundTileType> availableGroundTileTypes = SpawnRules.RulesForType(currentLastPlatform.groundTileType);
            List<GameObject> filteredPlatforms =
                _finalPlatformsPool.FindAll(p => availableGroundTileTypes.Contains(p.GetComponent<SpawnableGroundTile>().groundTileType));
            int idx = Random.Range(0, filteredPlatforms.Count);
            GameObject selectedPlatform = filteredPlatforms[idx];
            return selectedPlatform;
        }

        private static IEnumerable<SpawnableGroundTile> GetSortedVisibleSpawnableGroundTiles(bool desc) {
            return !desc
                ? FindObjectsOfType<SpawnableGroundTile>().OrderBy(p => p.UniqueId).ToArray()
                : FindObjectsOfType<SpawnableGroundTile>().OrderByDescending(p => p.UniqueId).ToArray();
        }

    }
}