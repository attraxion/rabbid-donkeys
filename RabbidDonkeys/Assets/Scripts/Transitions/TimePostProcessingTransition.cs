﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;
using System;

public class TimePostProcessingTransition : PostProcessingTransition {

    [Serializable]
    public class Config {
        public float timeForTransition = 3.0f;
    }

    public Config config;
    private PostProcessingBehaviour ppBeh;

    private void Awake() {
        base.Awake();
        ppBeh = GetComponent<PostProcessingBehaviour>();
    }

    public void RunTranstition() {
        if (ppBeh != null && ppBeh.profile != null) {
            TimeUpdatableProfile timeProf = ppBeh.gameObject.AddComponent<TimeUpdatableProfile>();
            timeProf.LerpOverTimeTo(ppBeh, futureProfile, config.timeForTransition);
            state.behavioursInTransit.Add(timeProf);
        }
    }

    public override void StopTransition() {
        base.StopTransition();
    }

//    void OnTriggerEnter(Collider other) {
    //        PostProcessingBehaviour ppBeh = other.GetComponentInChildren<PostProcessingBehaviour>();
    //        if (ppBeh != null && ppBeh.profile != null) {
    //            TimeUpdatableProfile timeProf = ppBeh.gameObject.AddComponent<TimeUpdatableProfile>();
    //            timeProf.LerpOverTimeTo(ppBeh, futureProfile, config.timeForTransition);
    //            state.behavioursInTransit.Add(timeProf);
    //        }
    //    }
}