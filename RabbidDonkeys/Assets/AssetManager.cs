﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssetManager : MonoBehaviour{

	private AssetContainer[] assets;
	
	
	// Use this for initialization
	void Start (){
		assets = GetComponentsInChildren<AssetContainer>(true);
		foreach (var asset in assets) {
			asset.RegisterToGM();
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void RegisterToGameManager(){
		
	}
}
