﻿using Managers;
using UnityEngine;

public class EnemyEntity : MonoBehaviour {

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.CompareTag("Player")) {
            print("Triggered player.");
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.gameObject.CompareTag("Player")) {
            print("Triggered player exited now.");
            GameManager.Instance.enemiesTransitioned += 1;
        }
    }
}