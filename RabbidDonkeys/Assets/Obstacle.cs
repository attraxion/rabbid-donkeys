﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour {

    private bool _passedThroughPlayer;

    void Update() {
        RaycastHit hit;
        Debug.DrawLine(transform.position, new Vector3(0, Mathf.Infinity, 0), Color.red);
        Physics.Raycast(transform.position + new Vector3(0, 50f, 0), Vector3.up, out hit, Mathf.Infinity, LayerMask.NameToLayer("Player"));
        if (hit.collider && hit.collider.gameObject.name == "Player") {
            Debug.Log("HIT PLAYER");
        }
    }
}