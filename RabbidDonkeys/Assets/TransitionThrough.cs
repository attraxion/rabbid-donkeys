﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using GameManager = Managers.GameManager;

public class TransitionThrough : MonoBehaviour {

    public bool transitionColliderEnabled = false;
    private CapsuleCollider _capsuleCollider;

    //layers are int ints here probably should make refactor
    [SerializeField] private List<int> _layersToCheck;

    private void Awake() {
        _capsuleCollider = GetComponent<CapsuleCollider>();
        if (_capsuleCollider == null) {
            _capsuleCollider = gameObject.AddComponent<CapsuleCollider>();
            _capsuleCollider.radius = 1.84f;
            _capsuleCollider.height = 4.34f;
            _capsuleCollider.center = new Vector3(0f, 2.25f, 0.49f);
            _capsuleCollider.isTrigger = true;
        }
    }

    private void Start() {
        _capsuleCollider.enabled = transitionColliderEnabled;
    }

    private void Update() {
        _capsuleCollider.enabled = transitionColliderEnabled;
    }

//    private void OnTriggerEnter(Collider other) {
//        if (_layersToCheck.Contains(other.gameObject.layer)) {
//            print("Triggered gameobject that has proper layer on.");
//        }
//    }
//
//    private void OnTriggerExit(Collider other) {
//        if (_layersToCheck.Contains(other.gameObject.layer)) {
//            print("Leaving transitioned organic life form!");
//            GameManager.Instance.enemiesTransitioned += 1;
//        }
//    }
}